# PCVR

Моя дипломная работа

  Если вам интересно как все то что было написано работает в действительности то можете перейти по [ ] [ссылке](https://drive.google.com/drive/folders/1CAqlSoFV0GMKw_DSr0F7mdZA8lUwPdss?usp=sharing) где есть демо-ролик с работоспособностью данного проекта и возможностями реализованными в нем.


Если в кратце описывать сам проект то данный проект был мной задуман как обучающее приложение с применением технологии VR, направленный на повышение компьютерной грамотности у учащихся старших классов и студентов институтов. 
В данном проекте реализованы следующие механики 

- процесс сборки песонального компьютера в среде VR из различных комплектующих.
- Во время соборки так же учитывается совместимость платформ (AMD / Intel  и тд.).
-  Оценка работоспособности сборки перед запуском
- Механника разгона ЦП, характеристики ЦП влияют на его производительность, но система не реагирует на аномальные значения, и нет системы ошибок виндов (синий экран смерти).
- Механика оценки производительности ПК на основе установленных комплектующих в процессе сборки.

# PCVR
My Diplom Work
 If you are interested in how everything that was written works in reality, then you can follow the [ ] [link](https://drive.google.com/drive/folders/1CAqlSoFV0GMKw_DSr0F7mdZA8lUwPdss?usp=sharing) where there is a demo video with the performance of this project and the capabilities implemented in it.


To briefly describe the project itself, this project was conceived by me as an educational application using VR technology, aimed at improving computer literacy among high school students and students of institutes.
The following mechanics are implemented in this project

- the process of assembling a personal computer in a VR environment from various components.
- During assembly, the compatibility of platforms (AMD / Intel, etc.) is also taken into account.
- Assessment of the performance of the assembly before launch
- CPU overclocking mechanics, CPU characteristics affect its performance, but the system does not respond to abnormal values, and there is no Windows error system (blue screen of death)
